function myFunction() {
  
var form = FormApp.create('Название для файла формы. 2018');
  
form.setDescription('Данный анонимный опрос проводится для контроля качества образовательного процесса и оценки восприятия учащимися его элементов. Опрос состоит из разделов - по одному на каждую учебную дисциплину прошлого семестра. Комментарий в свободной форме может быть введен в последнем разделе опроса.');
form.setProgressBar(true);

semestr6(form);

form.addPageBreakItem()
    .setTitle('Комментарии к организации учебного процесса');
  
form.addParagraphTextItem()
    .setTitle('Комментарий в свободной форме');

Logger.log('Published URL: ' + form.getPublishedUrl());
Logger.log('Editor URL: ' + form.getEditUrl());
  
}


function semestr6(form) {

addDiscipl(form, 'Теория поршневых и комбинированных двигателей');
addDiscipl(form, 'Управление в технических системах');
addDiscipl(form, 'Философия');
addDiscipl(form, 'Численные методы');
addDiscipl(form, 'Динамика двигателей');
addDiscipl(form, 'Иностранный язык');
addDiscipl(form, 'Механика жидкости и газа. Часть 2');
addDiscipl(form, 'Электротехника и электроника');

addKP(form,'Детали машин');

addPraxis(form, 'Организационно-технологическая практика');

}

  
function addDiscipl(fo, name) {
  
fo.addPageBreakItem()
    .setTitle('Дисциплина: '+name);
  
fo.addScaleItem()  
    .setTitle('Оцените актуальность учебного материала дисциплины')
    .setBounds(1, 5)
    .setLabels('Низкая', 'Высокая');

fo.addScaleItem()  
    .setTitle('Оцените глубину преподавания учебного материала дисциплины')
    .setBounds(1, 5)
    .setLabels('Низкая (разбирается поверхностно)', 'Высокая (разбирается детально)');

fo.addScaleItem()  
    .setTitle('Оцените сложность материала дисциплины')
    .setBounds(1, 5)
    .setLabels('Низкая', 'Очень высокая');

fo.addScaleItem()  
    .setTitle('Оцените объем активной работы студентов на занятиях (семинары, практикумы, лабораторные работы)')
    .setBounds(1, 5)
    .setLabels('Недостаточен', 'Чрезмерно велик');  
  
fo.addScaleItem()  
    .setTitle('Оцените объем самостоятельной работы (домашние задания; рефераты; проработка материала и т.п.)')
    .setBounds(1, 5)
    .setLabels('Недостаточен', 'Чрезмерно велик');
  
fo.addSectionHeaderItem()
    .setTitle('Ведущий преподаватель');
  
fo.addTextItem()
    .setTitle('Укажите фамилию ведущего преподавателя');
  
fo.addScaleItem()  
    .setTitle('Оцените качество проведения занятий')
    .setBounds(1, 5)
    .setLabels('Очень плохо', 'Отлично');
  
fo.addScaleItem()  
    .setTitle('Оцените уровень использования мультимедийных и информационных технологий')
    .setBounds(1, 5)
    .setLabels('Недостаточный', 'Избыточный');
  
fo.addScaleItem()  
    .setTitle('Оцените адекватность оценки преподавателем итогов освоения дисциплины студентами')
    .setBounds(1, 5)
    .setLabels('Требования к студентам занижены', 'Требования к студентам завышены');
  
}






function addPraxis(fo, name) {
  
  fo.addPageBreakItem()
    .setTitle('Практика: '+name);
  
  fo.addTextItem()
    .setTitle('Укажите место проведения');
  
  fo.addScaleItem()  
    .setTitle('Деятельность организации прохождения практики профилю осваиваемой профессии')
    .setBounds(1, 5)
    .setLabels('Не соответствует', 'Полностью соответствует');
  
  fo.addScaleItem()  
    .setTitle('Оцените качество организации проведения практики со стороны университета')
    .setBounds(1, 5)
    .setLabels('Неудовлетворительно', 'Отлично');
  
  fo.addScaleItem()  
    .setTitle('Оцените качество организации проведения практики со стороны организации прохождения практики')
    .setBounds(1, 5)
    .setLabels('Неудовлетворительно', 'Отлично');
  
  fo.addCheckboxItem()
    .setTitle('Охарактеризуйте свою преимущественную деятельность при прохождении практики')
    .setChoiceValues(['выполнялись специально подготовленные для практикантов учебные задачи',
                      'выполнялись действительные рабочие задачи организации в рамках квалификации практикантов',
                     'выполнялись действительные рабочие задачи, не требующие квалификации',
                     'выполнялась имитация бурной деятельности'])
    .showOtherOption(true);
  
  fo.addCheckboxItem()
    .setTitle('Оцените результаты прохождения практики')
    .setChoiceValues(['приобретены профессиональные навыки',
                      'приобретены профессиональные знания',
                      'приобретены полезные навыки общего характера',
                      'приобретены полезные знания общего характера',
                      'положительные результаты отсутствуют']);
  
  }





function addKP(fo, name) {
  
  fo.addPageBreakItem()
    .setTitle('Курсовой проект: '+name);
  
  fo.addScaleItem()  
    .setTitle('Оцените сложность выполнения проекта')
    .setBounds(1, 5)
    .setLabels('Низкая', 'Очень высокая');
  
  fo.addMultipleChoiceItem()
    .setTitle('Оцените адекватность содержания проекта реальным задачам осваиваемой профессии')
    .setChoiceValues(['полностью соответствует',
                      'отражает задачи в упрощенной форме',
                      'не соответствует']);
  
   fo.addMultipleChoiceItem()
    .setTitle('Оцените объем творческой составляющей содержания проекта')
    .setChoiceValues(['свыше 50 %',
                      'от 50 до 10 %',
                      'до 10 %']); 
   
}

#!/bin/bash

# Проверка на пустой вызов
if [ $# -eq 0 ]
  then
    echo "Укажите в коммандной строке через пробел имена файлов, на основе которых должна быть построена таблица."
    echo "Варианты:"
    find ./ -mindepth 1 -maxdepth 2 -type d -exec ls "{}" \; | sort | uniq
    exit
fi


# Перечень каталогов для обработки
dirs=`find ./ -mindepth 1 -maxdepth 2 -type d`

# Функция выводящая столбец
function column {
           echo '<td>' >> customTable.html
           sed 's/\r$//; /\uFEFF/d; / *%/d; /^ *$/d' "$1" >> customTable.html
           echo '</td>' >> customTable.html
}

# Шапка хтмл-документа
echo '<html>' > customTable.html
echo '<head>' >> customTable.html
echo '<meta charset="utf-8" />' >> customTable.html
echo '<title>Custom Table</title>'>> customTable.html
echo '</head>'>> customTable.html
echo '<body>'>> customTable.html
echo "<h1>Custom Table</h1>" >> customTable.html
echo '<table border="1">' >> customTable.html

# Перебор по папкам
for k in $dirs
        do
           echo '<tr>' >> customTable.html

           for c in $@
                do
                column "$k/$c"
                done
           
           echo '</tr>' >> customTable.html
        done

# Подвал хтмл-документа
echo '</table>' >> customTable.html
echo '</body>'>> customTable.html
echo '</html>'>> customTable.html

echo "Результат см. в файле customTable.html"

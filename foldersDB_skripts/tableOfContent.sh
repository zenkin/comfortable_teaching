#!/bin/bash

# Скрипт выводит для текущего каталога таблицу-сводку по наличию всех файлов 1 уровня в каждой из подпапок

dirs=`find ./ -mindepth 1 -maxdepth 2 -type d -exec basename {} \;`
alls=`find ./ -mindepth 1 -maxdepth 2 -type d -exec ls "{}" \; | sort | uniq`

#echo $alls
#echo $dirs


# Шапка csv-файла
echo -n "Каталог, "
for k in $alls
        do
                  echo -n $k ", "
        done
echo


for j in $dirs
do
	echo -n "$j"", "
	for k in $alls
	do
		if [ -f "$j/$k" ]; then
  		  echo -n "1, "
		  else
		echo -n "0, "

		fi
	
	done
	echo
done

exit 0

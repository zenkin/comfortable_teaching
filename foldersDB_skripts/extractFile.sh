#!/bin/bash

# Проверка на пустой вызов
if [ $# -eq 0 ]
  then
    echo "Укажите в коммандной строке имя файла, которые хотите извлечь из базы."
    echo "Варианты:"
    find ./ -mindepth 1 -maxdepth 2 -type d -exec ls "{}" \; | sort | uniq
    exit
fi

mkdir -p ./extract
rm -rf ./extract/*

# Перечень каталогов для обработки
dirs=`find ./ -mindepth 1 -maxdepth 2 -type d`

# Перебор по папкам
for k in $dirs
        do
           if [ -f "$k/$1" ]
                then
                cp "$k/$1" "./extract/`basename "$k"`_$1"
           fi
        done

echo "Результат см. в директории extract"
